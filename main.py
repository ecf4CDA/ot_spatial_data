import sqlalchemy as sql
import pyodbc
import pandas as pd
from flask import Flask, request, url_for
import json
import shapely.wkt

app = Flask(__name__, 
    static_url_path='',
    static_folder=''
            )

engine = sql.create_engine(
    r"mssql+pyodbc://@local?Trusted_Connection=yes"
)

metadata = sql.MetaData(bind=None)

table = sql.Table(
    'test_geom',
    metadata,
    autoload=True,
    autoload_with=engine
)

@app.route('/html_test')
def serve_index():
    return app.send_static_file('html_test.html')

@app.route('/get/get_data')
def get_data():
    query = sql.select([table.columns.ACTIVITY,
                    table.columns.SHAPE]
                    )

    with engine.connect() as conn:
        data = conn.execute(query).fetchall()
        activities = [x for x,y in data]
        shapes = [y for x,y in data]

    x_plot = []
    y_plot = []
    for shape in shapes:
        shape = shapely.wkt.loads(shape)
        x,y = shape.exterior.xy
        x = list(x)
        y = list(y)
        x.append(x[0])
        y.append(y[0])
        x_plot.append(x)
        y_plot.append(y)
    print(x_plot)
    return json.dumps({'activity': activities,
             'shape_x': x_plot,
             'shape_y': y_plot})

with app.test_request_context():
    print(url_for('get_data'))

if __name__=='__main__':
    get_data()

