
// set the dimensions and margins of the graph
var margin = {top: 10, right: 30, bottom: 30, left: 60},
    width = 800 - margin.left - margin.right,
    height = 700 - margin.top - margin.bottom;

// append the svg object to the body of the page


d3.csv('data.csv').then(
   function(data) {
      console.log(data);
      for (i=0; i<data.length; i++) {
         var d = JSON.parse(data[i].ARCS);
         var to_plot = [];
         for (j=0; j<d.length; j++) {
            var x = d[j][0];
            var y = d[j][1];
            to_plot.push({'x':x, 'y':y});
         };
         data[i].PLOT = to_plot;
      };
      console.log(data[0]);

      // Process data to make it easier for D3 to construct axes
      var processed_data = data.reduce(function(arr, obj) {
         return arr.concat(obj.PLOT.map(function(x) {return x}))
       }, []);
       //console.log(processed_data);

   var svg = d3.select("#my_dataviz")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")")
      .call(d3.zoom().on("zoom", function () {
         d3.selectAll("svg").attr("transform", d3.event.transform)
      }));
      
   function zoomed(transform) {
      svg.attr("transform", transform);
      };   
   // Standard zoom behavior:
   var zoom = d3.zoom()
   .on("zoom", zoomed);

   svg.call(zoom);
      // Add X axis
   var x = d3.scaleLinear()
      .domain(d3.extent(processed_data, function(d) { return +d.x; }))
      .range([ 0, width ]);
   svg.append("g")
      .attr("transform", "translate(0," + height + ")");
      //.call(d3.axisBottom(x));

   // Add Y axis
   var y = d3.scaleLinear()
      .domain(d3.extent(processed_data, function(d) { return +d.y; }))
      .range([ height, 0 ]);
   svg.append("g")
      //.call(d3.axisLeft(y));

   console.log(data[3000].PLOT);

   draw_shape = function(svg, row) {
      var data = row.PLOT;
      svg.selectAll('.shapes')
      .data([data])
      .enter().append('polygon')
      .attr("fill", "none")
      .attr("stroke", "red")
      .attr("stroke-width", "0.5")
      .attr("points", function(d){
         return d.map(function(d) {return [x(d.x),y(d.y)].join(",");
         }).join(" ");
      });
   };

   // Draw the lines
   for (i=0; i<data.length; i++) {
      draw_shape(svg, data[i]);
      };
   }
)


