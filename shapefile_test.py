import shapefile
import pygeoif

import pandas as pd
import sqlalchemy as sql

sf = shapefile.Reader(r".\OT spatial exploration\CAD_SCHED_Shapefiles_TopoJSON_PBI\Scenario3_Verandah&UCL_Speed_15mpermo.shp")
print(sf)
shapes = []
info = []
for entry in sf:
    # shape = pygeoif.geometry.as_shape(shapefile.POLYGONZ(entry))
    shapes.append(pygeoif.geometry.as_shape(entry.shape).to_wkt())
    info.append(list(entry.record))

print(type(shapes[0]))
print(dir(shapes[0]))


col_names = ['ACTIVITY',	
             'BELL_DRAW',
        	 'DESCRIPTIO',
             'DRAWBELL_C',	
             'EXCAVATION',	
             'FINISH',	
             'ID',	
             'METRES',	
             'MINE',	
             'MINE_AREA',	
             'NAME',	
             'P0_CAVE',	
             'PANEL',	
             'PRI_NUM',	
             'PRIORITY',	
             'START',	
             'D_AREA',
             'SHAPE'
]

for i in range(len(info)):
    info[i].append(shapes[i])
df = pd.DataFrame().from_records(info, columns=col_names)
# df.columns = col_names
print(df.head())

# cnxn = pyodbc.connect("DSN=local")

# conn_str = (
#     r'DRIVER={SQL Server};'
#     r'SERVER=ECF-5CD102GP4Q;'
#     r'DATABASE=master;'
#     r'Trusted_Connection=yes;'
# )
# cnxn = pyodbc.connect(conn_str)

engine = sql.create_engine(
    r"mssql+pyodbc://@local?Trusted_Connection=yes"
)



df.to_sql("test_wkt",
          engine, if_exists='replace')